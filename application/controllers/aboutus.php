<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Aboutus extends CI_Controller {

	public function index()
    {
        $data = array();
        $data['page'] = 'aboutus';
        $dataContent = array();
        $data['content'] = $this->load->view('aboutus_index',$dataContent,true);
		$this->load->view('masterpage', $data);
	}
    
    public function teacher($teacher_id = 0)
    {
        if($teacher_id <= 0 || !is_numeric($teacher_id)){
            redirect('aboutus/index');
        }
        $data = array();
        $data['page'] = 'aboutus';
        $dataContent = array();
        $dataContent['teacher_id'] = $teacher_id;
        $data['content'] = $this->load->view('aboutus_detail',$dataContent,true);
		$this->load->view('masterpage', $data);
    }
}

/* End of file aboutus.php */
/* Location: ./application/controllers/aboutus.php */