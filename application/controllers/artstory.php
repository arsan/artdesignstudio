<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Artstory extends CI_Controller {

	public function index()
    {
        $data = array();
        $data['page'] = 'artstory';
        $dataContent = array();
        $data['content'] = $this->load->view('artstory',$dataContent,true);
		$this->load->view('masterpage', $data);
	}
    
    public function detail($art_story_id = 0)
    {
        
        $data = array();
        $data['page'] = 'artstory';
        $dataContent = array();
        $dataContent['art_story_id'] = $art_story_id;
        $data['content'] = $this->load->view('artstory_detail',$dataContent,true);
		$this->load->view('masterpage', $data);
        
    }
}

/* End of file artstory.php */
/* Location: ./application/controllers/artstory.php */