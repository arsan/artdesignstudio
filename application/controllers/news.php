<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class News extends CI_Controller {

    public function index()
    {
        $data = array();
        $data['page'] = 'news';
        $dataContent = array();
        $data['content'] = $this->load->view('news',$dataContent,true);
        $this->load->view('masterpage', $data);
    }

    public function detail($news_id = 0)
    {
        if(is_numeric($news_id) && $news_id > 0)
        {
            $data = array();
            $data['page'] = 'news';
            $dataContent = array();
            $dataContent['news_id'] = $news_id;
            $data['content'] = $this->load->view('news_detail',$dataContent,true);
            $this->load->view('masterpage', $data);
        }
        else
        {
            redirect('news');
        }
    }
}

/* End of file news.php */
/* Location: ./application/controllers/news.php */