<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
    {
        $data = array();
        $data['page'] = 'home';
        $dataContent = array();
        $data['content'] = $this->load->view('home',$dataContent,true);
		$this->load->view('masterpage', $data);
	}
}

/* End of file home.php */
/* Location: ./application/controllers/home.php */