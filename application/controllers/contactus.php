<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contactus extends CI_Controller {

    public function index()
    {
        $data = array();
        $data['page'] = 'contactus';
        $dataContent = array();
        $data['content'] = $this->load->view('contactus',$dataContent,true);
        $this->load->view('masterpage', $data);
    }

    public function send()
    {
        if(!$this->SpamModel->check_spam($_POST['inputEmail']) 
           && !$this->SpamModel->check_spam($_POST['inputName'])
           && !$this->SpamModel->check_spam($_POST['inputPhone'])
           && !$this->SpamModel->check_spam($_POST['inputMessage']))
        {
            $data = array(
                'email' => $_POST['inputEmail'],
                'name' => $_POST['inputName'],
                'phone' => $_POST['inputPhone'],
                'message' => $_POST['inputMessage'],
                'parent_id' => 1
            );

            $this->db->set('create_date', 'now()', FALSE);
            $this->db->set('update_date', 'now()', FALSE);
            $this->db->insert('tbl_contact_us_list', $data);
            redirect('contactus/thankyou');
        }
        else
        {
            redirect('contactus/index');
        }
    }

    public function thankyou()
    {
        $data = array();
        $data['page'] = 'contactus';
        $dataContent = array();
        $data['content'] = $this->load->view('contactus_thankyou',$dataContent,true);
        $this->load->view('masterpage', $data);
    }
}

/* End of file contactus.php */
/* Location: ./application/controllers/contactus.php */