<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Course extends CI_Controller {

	public function index()
    {
        $data = array();
        $data['page'] = 'course';
        $dataContent = array();
        $data['content'] = $this->load->view('course',$dataContent,true);
		$this->load->view('masterpage', $data);
	}
}

/* End of file course.php */
/* Location: ./application/controllers/course.php */