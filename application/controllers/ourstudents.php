<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ourstudents extends CI_Controller {

    public function index()
    {
        $data = array();
        $data['page'] = 'ourstudents';
        $dataContent = array();
        $data['content'] = $this->load->view('ourstudents',$dataContent,true);
        $this->load->view('masterpage', $data);
    }

    public function detail($student_id = 0)
    {
        if(is_numeric($student_id) && $student_id > 0)
        {
            $data = array();
            $data['page'] = 'ourstudents';
            $dataContent = array();
            $dataContent['student_id'] = $student_id;
            $data['content'] = $this->load->view('ourstudents_detail',$dataContent,true);
            $this->load->view('masterpage', $data);
        }
        else
        {
            redirect('ourstudents');
        }
    }
}

/* End of file ourstudents.php */
/* Location: ./application/controllers/ourstudents.php */