<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gallery extends CI_Controller {

    public function index()
    {
        $data = array();
        $data['page'] = 'gallery';
        $dataContent = array();
        $data['content'] = $this->load->view('gallery',$dataContent,true);
        $this->load->view('masterpage', $data);
    }

    public function detail($gallery_id = 0){
        if(is_numeric($gallery_id) && $gallery_id > 0)
        {
            $data = array();
            $data['page'] = 'gallery';
            $dataContent = array();
            $dataContent['gallery_id'] = $gallery_id;
            $data['content'] = $this->load->view('gallery_detail',$dataContent,true);
            $this->load->view('masterpage', $data);
        }
        else   
        {
            redirect('gallery');
        }
    }
}

/* End of file gallery.php */
/* Location: ./application/controllers/gallery.php */