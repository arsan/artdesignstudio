<?php

class SpamModel extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function check_spam($postStr, $require = true) {
        $returnVal = false;
        if ($require && trim($postStr) == "") {
            $returnVal = true;
        }
        if ($postStr != strip_tags($postStr) || $postStr != str_replace("http://", "", $postStr)) {
            $returnVal = true;
        }
        //if return true equal spam
        //if return false equal not spam
        return $returnVal;
    }

}

?>
