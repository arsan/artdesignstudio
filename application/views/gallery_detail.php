<div class="gallery_detail">
    <div class="row">
        <div class="col-sm-12">
            <h1>GALLERY</h1>
            <div class="row">
                <div class="col-sm-12">
                    <div class="well">
                        <?php $gallery = $this->MotherModel->getDynamicSingleContent(17,1,$gallery_id);?>
                        <?php echo $gallery->detail;?>
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Photo</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static"><img src="<?php echo $gallery->artist_photo;?>" alt="" class="img-responsive" width="250"/></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Artist</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static"><?php echo $gallery->artist;?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Media</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static"><?php echo $gallery->media;?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Year</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static"><?php echo $gallery->years;?></p>
                                </div>
                            </div>
                            <?php /*?>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Technique</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static"><?php echo $gallery->technique;?></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Inspiration</label>
                                <div class="col-sm-10">
                                    <p class="form-control-static"><?php echo $gallery->inspiration;?></p>
                                </div>
                            </div>
                            <?php */?>
                        </form>
                        <a href="<?php echo site_url('gallery');?>" class="btn btn-default btn-block" role="button">< Back</a>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>