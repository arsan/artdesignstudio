
<?php $teacher = $this->MotherModel->getDynamicSingleContent(8,1,$teacher_id);?>
<div class="aboutus-detail">
    <div class="row">
        <div class="col-md-6">
            <div class="photo">
                <img src="<?php echo $teacher->image;?>" alt="" class="img-responsive">
                <p><?php echo $teacher->name;?></p>
            </div>
        </div>
        <div class="col-md-6">
            <?php echo $teacher->profile;?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <h2 class="ef4036">ผลงงานการ Paint</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?php $works = $this->MotherModel->getDynamicContent(9,1,$teacher_id);?>
            <?php foreach ($works->result_array() as $row){?>
            <div class="col-md-3 col-sm-6 col-xs-6 thumb">
                <a href="<?php echo $row['image'];?>" class="fancybox" rel="group">
                    <img src="<?php echo $row['thumb'];?>" alt="" class="img-responsive">
                </a>
            </div>
            <?php }?>
        </div>
    </div>
</div>
