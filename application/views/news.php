<div class="news">
    <div class="row">
        <div class="col-sm-12">
            <h1>UPDATE : Proud to Present</h1>
            <div class="row">
                <?php $newsList = $this->MotherModel->getDynamicContent(11,1,1);?>
                <?php foreach ($newsList->result_array() as $row){?>
                <div class="col-md-3 col-sm-6 col-xs-6 news-item">
                    <div class="thumb"><img src="<?php echo $row['thumb'];?>" alt="" class="img-responsive"></div>
                    <div class="content">
                        <p>
                            <b class="name"><?php echo $row['title'];?></b>
                            <br/><b><?php echo $row['expand'];?></b>
                            <br/><?php echo $row['sub_title'];?>
                        </p>
                        <p class="date"><?php echo $row['date'];?></p>
                    </div>
                    <a href="<?php echo site_url('news/detail/'.$row['news_list_id']);?>" class="more">Read more</a>
                </div>
                <?php }?>
            </div>
        </div>
    </div>
</div>