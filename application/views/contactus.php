
<?php $contact = $this->MotherModel->getStaticContent(7,1);?>
<div class="contactus">
    <div class="row">
        <div class="col-sm-12">
            <h1>CONTACT US</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <iframe src="<?php echo $contact->google_map;?>" width="100%" height="450" frameborder="0" style="border:0"></iframe>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-7 col-sm-offset-1">
            <h2>Contact Form</h2>
            <form role="form" action="<?php echo site_url('contactus/send');?>" method="post">
                <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name="inputEmail" placeholder="Enter email" data-validation="email" data-validation-error-msg="Please input valid email.">
                </div>
                <div class="form-group">
                    <label for="exampleInputName">Name</label>
                    <input type="text" class="form-control" id="exampleInputName" name="inputName" placeholder="Enter name" data-validation="required">
                </div>
                <div class="form-group">
                    <label for="exampleInputPhone">Phone Number</label>
                    <input type="text" class="form-control" id="exampleInputPhone" name="inputPhone" placeholder="Enter phone number" data-validation="length number" data-validation-length="10-10" data-validation-error-msg="Please input valid mobile phone.">
                </div>
                <div class="form-group">
                    <label for="exampleInputMessage">Message</label>
                    <textarea class="form-control" rows="3" class="form-control" id="exampleInputEmail1" name="inputMessage" placeholder="Enter email" data-validation="required"></textarea>
                </div>
                <button type="submit" class="btn btn-default">SEND MESSAGE</button>
            </form>
        </div>
        <div class="col-sm-3">
            <h3>Address</h3>
            <?php echo $contact->address;?>
            <h3>Contact</h3>
            <?php echo $contact->contact;?>
    </div>
</div>