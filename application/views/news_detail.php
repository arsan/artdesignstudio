<div class="news_detail">
    <div class="row">
        <div class="col-sm-12">
            <h1>UPDATE : Proud to Present</h1>
            <div class="row">
                <div class="col-sm-12">
                    <?php $news = $this->MotherModel->getDynamicSingleContent(11,1,$news_id);?>
                    <div class="well">
                        <h2 class="ef4036"><?php echo $news->title;?></h2>
                        <h2 class="black"><?php echo $news->expand;?></h2>
                        <h3><?php echo $news->sub_title;?></h3>
                        <?php echo $news->detail;?>
                        <a href="<?php echo site_url('news');?>" class="btn btn-default btn-block" role="button">&lt; Back</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>