<div class="gallery_detail">
    <div class="row">
        <div class="col-sm-12">
            <h1>Art Story</h1>
            <div class="row">
                <div class="col-sm-12">
                    <div class="well">
                        <?php $story = $this->MotherModel->getDynamicSingleContent(14,1,$art_story_id);?>
                        <h2><?php echo $story->title;?></h2>
                        <?php echo $story->detail;?>
                        <a href="<?php echo site_url('artstory');?>" class="btn btn-default btn-block" role="button">< Back</a>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>