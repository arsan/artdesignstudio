<div class="gallery">
    <div class="row">
        <div class="col-sm-12">
            <h1>GALLERY</h1>
            <div class="row">
                <div class="col-sm-12 slider">
                   <?php $slides = $this->MotherModel->getDynamicContent(12,1,1);?>
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                           <?php $i = 0;foreach ($slides->result_array() as $row){?>
                            <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i;?>" class="<?php echo ($i==0)?'active':'';?>"></li>
                           <?php $i++;}?>
                           <?php /*?>
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            <?php */?>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                           <?php $i = 0;foreach ($slides->result_array() as $row){?>
                            <div class="item <?php echo ($i==0)?'active':'';?>">
                                <img src="<?php echo $row['image'];?>" alt="<?php echo $row['title'];?>">
                                <div class="carousel-caption"></div>
                            </div>
                           <?php $i++;}?>
                           <?php /*?>
                            <div class="item active">
                                <img src="<?php echo base_url('assets/imgs/gallery/slider.jpg');?>" alt="...">
                                <div class="carousel-caption">
                                    ...
                                </div>
                            </div>
                            <div class="item">
                                <img src="<?php echo base_url('assets/imgs/gallery/slider.jpg');?>" alt="...">
                                <div class="carousel-caption">
                                    ...
                                </div>
                            </div>
                            <div class="item">
                                <img src="<?php echo base_url('assets/imgs/gallery/slider.jpg');?>" alt="...">
                                <div class="carousel-caption">
                                    ...
                                </div>
                            </div><?php */?>
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="row">
                <?php $gallery = $this->MotherModel->getDynamicContent(17,1,1);?>
                <?php foreach ($gallery->result_array() as $row){?>
                <div class="col-md-3 col-sm-6 col-xs-6 thumb-item">
                    <a href="<?php echo site_url('gallery/detail/'.$row['home_gallery_id']);?>"><img src="<?php echo $row['thumb'];?>" alt="" class="img-responsive"></a>
                </div>
                <?php }?>
            </div>
        </div>
    </div>
</div>