<div class="ourstudents">
    <div class="row">
        <div class="col-sm-12">
            <h1>OUR STUDENTS</h1>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">
                        <?php $newsList = $this->MotherModel->getDynamicContent(18,1,0);?>
                        <?php foreach ($newsList->result_array() as $row){?>
                        <div class="col-md-3 col-sm-4 col-xs-6 news-item">
                            <div class="thumb">
                                <a href="<?php echo site_url('ourstudents/detail/'.$row['student_id']);?>">
                                    <img src="<?php echo $row['thumb'];?>" alt="" class="img-responsive">
                                </a>
                            </div>
                            <div class="content">
                                <p>
                                    <a href="<?php echo site_url('ourstudents/detail/'.$row['student_id']);?>">
                                        <span class="fullname"><?php echo $row['fullname'];?></span>
                                        <br/><span class="school"><?php echo $row['school'];?></span>
                                    </a>
                                </p>
                            </div>
                        </div>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <br/>
</div>