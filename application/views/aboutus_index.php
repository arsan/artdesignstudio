<div class="aboutus">
    <?php $aboutus = $this->MotherModel->getStaticContent(2,1);?>
    <div class="row aboutus-banner">
        <div class="col-xs-12">
            <img src="<?php echo $aboutus->banner;?>" alt="" class="img-responsive">
            <h1><?php echo $aboutus->banner_text;?></h1>
        </div>
    </div>
    <div class="row aboutus-content">
        <div class="col-lg-10 col-md-9 col-sm-12 article">
            <?php echo $aboutus->detail;?>
        </div>
        <div class="col-lg-2 col-md-3 col-sm-12 teacher">
            <div class="row">
               <?php $teacher = $this->MotherModel->getDynamicContent(8,1,1);?>
               <?php foreach ($teacher->result_array() as $row){?>
                <div class="col-md-12 col-sm-4 col-xs-6">
                    <h2>Instructor</h2>
                    <a href="<?php echo site_url('aboutus/teacher/'.$row['teacher_id']);?>"><img src="<?php echo $row['thumb'];?>" alt="" class="img-responsive"></a>
                    <p><?php echo $row['sub_title'];?></p>
                </div>
               <?php }?>
            </div>
        </div>
    </div>
</div>