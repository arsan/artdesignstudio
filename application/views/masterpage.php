<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="<?php echo $this->ConfigModel->getMetaDescription(); ?>">
        <meta name="keyword" content="<?php echo $this->ConfigModel->getMetaKeyword(); ?>">
        <title><?php echo $this->ConfigModel->getWebsiteName(); ?></title>
        <link rel="icon" href="<?php echo $this->ConfigModel->getFavicon(); ?>">
        <!-- Normalize -->
        <link href="<?php echo base_url('assets/css/normalize.css');?>" rel="stylesheet">
        <!-- Bootstrap -->
        <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
        <!-- Google Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
        <!-- Style -->
        <link href="<?php echo base_url('assets/css/style.css');?>" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
        <?php echo $this->ConfigModel->getGoogleAnalytic(); ?>
    </head>
    <body>
        <?php echo $this->ConfigModel->getGoogleRemarketing(); ?>
        <?php $contact = $this->MotherModel->getStaticContent(7,1);?>
        <div class="container">
            <header>
                <div class="row">
                    <div class="col-md-5 col-sm-12">
                        <a href="<?php echo site_url();?>" class="logo"><img src="<?php echo base_url('assets/imgs/logo.png');?>" alt="ART SQUARE STUDIO" class="img-responsive"></a>
                    </div>
                    <div class="col-md-7 col-sm-12">
                        <div class="tel">
                            <b>CALL</b> <a href="tel:<?php echo $contact->tel;?>"><?php echo $contact->tel;?></a>
                        </div>
                        <div class="clearfix">
                            <?php /*?>
                            <nav class="lang">
                                <ul class="clearfix">
                                    <li><a href="">TH</a></li>
                                    <li><a href="">ENG</a></li>
                                </ul>
                            </nav>
                            <?php */?>
                            <nav>
                                <select name="nav-mobile" id="nav-mobile" class="form-control visible-xs">
                                    <option value="<?php echo site_url('home');?>" <?php echo ($page=='home')?'selected="selected"':'';?>>HOME</option>
                                    <option value="<?php echo site_url('aboutus');?>" <?php echo ($page=='aboutus')?'selected="selected"':'';?>>ABOUT US</option>
                                    <option value="<?php echo site_url('course');?>" <?php echo ($page=='course')?'selected="selected"':'';?>>COURSE</option>
                                    <option value="<?php echo site_url('news');?>" <?php echo ($page=='news')?'selected="selected"':'';?>>NEWS</option>
                                    <option value="<?php echo site_url('gallery');?>" <?php echo ($page=='gallery')?'selected="selected"':'';?>>GALLERY</option>
                                    <option value="<?php echo site_url('ourstudents');?>" <?php echo ($page=='ourstudents')?'selected="selected"':'';?>>OUR STUDENTS</option>
                                    <option value="<?php echo site_url('artstory');?>" <?php echo ($page=='artstory')?'selected="selected"':'';?>>ART STORY</option>
                                    <option value="<?php echo site_url('contactus');?>" <?php echo ($page=='contactus')?'selected="selected"':'';?>>CONTACT US</option>
                                </select>
                                <ul class="clearfix hidden-xs">
                                    <li><a href="<?php echo site_url('home');?>" class="<?php echo ($page=='home')?'active':'';?>">HOME</a></li>
                                    <li><a href="<?php echo site_url('aboutus');?>" class="<?php echo ($page=='aboutus')?'active':'';?>">ABOUT US</a></li>
                                    <li><a href="<?php echo site_url('course');?>" class="<?php echo ($page=='course')?'active':'';?>">COURSE</a></li>
                                    <li><a href="<?php echo site_url('news');?>" class="<?php echo ($page=='news')?'active':'';?>">UPDATE</a></li>
                                    <li><a href="<?php echo site_url('gallery');?>" class="<?php echo ($page=='gallery')?'active':'';?>">GALLERY</a></li>
                                    <li><a href="<?php echo site_url('ourstudents');?>" class="<?php echo ($page=='ourstudents')?'active':'';?>">OUR STUDENTS</a></li>
                                    <li><a href="<?php echo site_url('artstory');?>" class="<?php echo ($page=='artstory')?'active':'';?>">ART STORY</a></li>
                                    <li><a href="<?php echo site_url('contactus');?>" class="<?php echo ($page=='contactus')?'active':'';?>">CONTACT US</a></li>    
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </header>
            <main>
                <?php echo $content;?>
            </main>
            <footer>
                <div class="row">
                    <div class="col-md-3 col-sm-12">
                        <h3>Map</h3>
                        <iframe src="<?php echo $contact->google_map;?>" width="100%" height="225" frameborder="0" style="border:0"></iframe>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <h3>About Us</h3>
                        <?php echo $contact->about_us;?>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <h3>Address</h3>
                        <?php echo $contact->address;?>
                    </div>
                    <div class="col-md-3 col-sm-12 social-network">
                        <h3>Social Network</h3>
                        <a href="https://www.facebook.com/artsquare.artandgallery" target="_blank"><img src="<?php echo base_url('assets/imgs/footer/icon-facebook.jpg');?>" alt=""></a>
                        <!--a href="#" target="_blank"><img src="<?php echo base_url('assets/imgs/footer/icon-twitter.jpg');?>" alt=""></a>
<a href="#" target="_blank"><img src="<?php echo base_url('assets/imgs/footer/icon-vimeo.jpg');?>" alt=""></a>
<a href="#" target="_blank"><img src="<?php echo base_url('assets/imgs/footer/icon-rss.jpg');?>" alt=""></a>
<a href="#" target="_blank"><img src="<?php echo base_url('assets/imgs/footer/icon-googleplus.jpg');?>" alt=""></a>
<a href="#" target="_blank"><img src="<?php echo base_url('assets/imgs/footer/icon-pinterest.jpg');?>" alt=""></a-->
                    </div>
                </div>
            </footer>
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
        <!-- Add mousewheel plugin (this is optional) -->
        <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/fancybox/lib/jquery.mousewheel-3.0.6.pack.js');?>"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
        <!-- Form Validator -->
        <script src="<?php echo base_url('assets/js/plugins/formvalidator/jquery.form-validator.min.js'); ?>"></script>
        <script>$.validate();</script>
        <!-- Add fancyBox -->
        <link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/fancybox/source/jquery.fancybox.css');?>" type="text/css" media="screen" />
        <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/fancybox/source/jquery.fancybox.js');?>"></script>
        <!-- Optionally add helpers - button, thumbnail and/or media -->
        <link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5');?>" type="text/css" media="screen" />
        <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5');?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6');?>"></script>

        <link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7');?>" type="text/css" media="screen" />
        <script type="text/javascript" src="<?php echo base_url('assets/js/plugins/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7');?>"></script>
        <script>
            $(function(){
                equalheight = function(container){

                    var currentTallest = 0,
                        currentRowStart = 0,
                        rowDivs = new Array(),
                        $el,
                        topPosition = 0;
                    $(container).each(function() {

                        $el = $(this);
                        $($el).height('auto')
                        topPostion = $el.position().top;

                        if (currentRowStart != topPostion) {
                            for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                                rowDivs[currentDiv].height(currentTallest);
                            }
                            rowDivs.length = 0; // empty the array
                            currentRowStart = topPostion;
                            currentTallest = $el.height();
                            rowDivs.push($el);
                        } else {
                            rowDivs.push($el);
                            currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
                        }
                        for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
                            rowDivs[currentDiv].height(currentTallest);
                        }
                    });
                }
                $('#nav-mobile').change(function(){
                    $(location).attr('href', $(this).val());
                });
                $(".fancybox").fancybox();
                $(window).load(function() {
                    equalheight('.update-item .content p.title');
                    equalheight('.update-item .content p.sub-title');
                    equalheight('.gallery-list p');
                    equalheight('.news-item .content');
                });

                $(window).resize(function(){
                    equalheight('.update-item .content p.title');
                    equalheight('.update-item .content p.sub-title');
                    equalheight('.gallery-list p');
                    equalheight('.news-item .content');
                });
                $('.carousel').carousel({
                    interval: 4000
                });
            });
        </script>
    </body>
</html>
