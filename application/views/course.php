<div class="course">
    <?php $course = $this->MotherModel->getStaticContent(3,1);?>
    <div class="row course-header">
        <div class="col-sm-12">
            <?php echo $course->header;?>
        </div>
    </div>
    <?php $courses = $this->MotherModel->getDynamicContent(10,1,1);?>
    <?php $i = 0;foreach ($courses->result_array() as $row){ $i++;?>
    <div class="row couse-list <?php echo ($i%2==0)?'right':'';?>" id="<?php echo $row['title'];?>">
        <div class="col-sm-12">
            <div class="row course-intro">
                <div class="col-md-4 course-intro-text">
                    <h2><?php echo $row['title'];?></h2> 
                    <h3>COURSE</h3> 
                    <p>
                        <?php echo $row['intro'];?>
                    </p>
                </div>
                <div class="col-md-8 course-intro-image">
                    <img src="<?php echo $row['image'];?>" alt="" class="img-responsive">
                </div>
            </div>
            <div class="row course-detail">
                <div class="col-xs-12">
                    <h2>
                        Course Detail
                    </h2>
                    <?php echo $row['detail'];?>
                </div>
            </div>
        </div>
    </div>
    <?php }?>
    <!--div class="row couse-list">
        <div class="col-sm-12">
            <div class="row course-intro">
                <div class="col-md-4 course-intro-text">
                    <h2>CHILDREN</h2> 
                    <h3>COURSE</h3> 
                    <p>
                        In the first few years of this new decade three trophies were added to the cabinet by José Mourinho's Real Madrid. The most notable was the 2011-12 Liga title, which they won with a record-breaking 100 points, the highest score achieved in the history of the championship at that stage, and 121 goals. They also beat Barcelona to clinch a Copa del Rey and a Supercopa de España.
                    </p>
                </div>
                <div class="col-md-8 course-intro-image">
                    <img src="<?php echo base_url('assets/imgs/course/course-1.jpg');?>" alt="" class="img-responsive">
                </div>
            </div>
            <div class="row course-detail">
                <div class="col-xs-12">
                    <h2>
                        Course Detail
                    </h2>
                    <p><b>In this era in the club's history there is a date that stands out above the rest: 24 May 2014. That day, Real Madrid conquered Europe again, claiming their eagerly awaited Tenth. At the beginning of the decade the Whites won three titles under José Mourinho. The Portuguese manager left at the end of the 2012-13 campaign and Florentino Pérez appointed Carlo Ancelotti. In his first season in charge of Madrid he won the Copa del Rey and the Champions League.</b> </p>
                    <p> In the first few years of this new decade three trophies were added to the cabinet by José Mourinho's Real Madrid. The most notable was the 2011-12 Liga title, which they won with a record-breaking 100 points, the highest score achieved in the history of the championship at that stage, and 121 goals. They also beat Barcelona to clinch a Copa del Rey and a Supercopa de España.
                    </p>
                    <p>
                        In June 2013, Carlo Ancelotti fulfilled his dream of managing Real Madrid, arriving with a résumé that included a dozen or so titles and around 20 campaigns as a manager. The Italian, accustomed to the pressure of big teams like Juventus, Milan, Chelsea and PSG, heads an exciting project. In his first bid for a title, he claimed the Whites' nineteenth Copa, beating Barcelona in the final 1-2 with goals from Di María and Bale.
                    </p>
                    <p>
                        On 24 May 2014 the Whites reclaimed the European Cup. With their 4-1 victory in Lisbon against Atlético they earned a tenth title for the club. Ramos, Bale, Marcelo and Cristiano sealed the win. In his first season, Ancelotti secured an unprecedented double in the history of Real Madrid: the Copa del Rey and the Champions League.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="row couse-list right">
        <div class="col-sm-12">
            <div class="row course-intro">
                <div class="col-md-4 course-intro-text">
                    <h2>PRIMARY</h2> 
                    <h3>COURSE</h3> 
                    <p>
                        In the first few years of this new decade three trophies were added to the cabinet by José Mourinho's Real Madrid. The most notable was the 2011-12 Liga title, which they won with a record-breaking 100 points, the highest score achieved in the history of the championship at that stage, and 121 goals. They also beat Barcelona to clinch a Copa del Rey and a Supercopa de España.
                    </p>
                </div>
                <div class="col-md-8 course-intro-image">
                    <img src="<?php echo base_url('assets/imgs/course/course-2.jpg');?>" alt="" class="img-responsive">
                </div>
            </div>
            <div class="row course-detail">
                <div class="col-xs-12">
                    <h2>
                        Course Detail
                    </h2>
                    <p><b>In this era in the club's history there is a date that stands out above the rest: 24 May 2014. That day, Real Madrid conquered Europe again, claiming their eagerly awaited Tenth. At the beginning of the decade the Whites won three titles under José Mourinho. The Portuguese manager left at the end of the 2012-13 campaign and Florentino Pérez appointed Carlo Ancelotti. In his first season in charge of Madrid he won the Copa del Rey and the Champions League.</b> </p>
                    <p> In the first few years of this new decade three trophies were added to the cabinet by José Mourinho's Real Madrid. The most notable was the 2011-12 Liga title, which they won with a record-breaking 100 points, the highest score achieved in the history of the championship at that stage, and 121 goals. They also beat Barcelona to clinch a Copa del Rey and a Supercopa de España.
                    </p>
                    <p>
                        In June 2013, Carlo Ancelotti fulfilled his dream of managing Real Madrid, arriving with a résumé that included a dozen or so titles and around 20 campaigns as a manager. The Italian, accustomed to the pressure of big teams like Juventus, Milan, Chelsea and PSG, heads an exciting project. In his first bid for a title, he claimed the Whites' nineteenth Copa, beating Barcelona in the final 1-2 with goals from Di María and Bale.
                    </p>
                    <p>
                        On 24 May 2014 the Whites reclaimed the European Cup. With their 4-1 victory in Lisbon against Atlético they earned a tenth title for the club. Ramos, Bale, Marcelo and Cristiano sealed the win. In his first season, Ancelotti secured an unprecedented double in the history of Real Madrid: the Copa del Rey and the Champions League.</p>
                </div>
            </div>
        </div>
    </div-->
    <div class="row course-footer">
        <div class="col-sm-12">
            <?php echo $course->footer;?>
        </div>
    </div>
</div>