<div class="ourstudents_detail">
    <div class="row">
        <div class="col-sm-12">
            <h1>OUR STUDENTS</h1>
            <div class="row">
                <?php $student = $this->MotherModel->getDynamicSingleContent(18,1,$student_id);?>
                <div class="col-md-4 col-md-offset-1 col-sm-5">
                    <div class="student-image"><img src="<?php echo $student->image;?>" alt="" class="img-responsive"></div>
                </div>
                <div class="col-md-6 col-sm-7">
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">ชื่อ-นามสกุล</label>
                            <div class="col-sm-9"><?php echo $student->fullname;?></div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">ชื่อเล่น</label>
                            <div class="col-sm-9"><?php echo $student->nickname;?></div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">อายุ</label>
                            <div class="col-sm-9"><?php echo $student->age;?></div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">โรงเรียน</label>
                            <div class="col-sm-9"><?php echo $student->school;?></div>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail3" class="col-sm-3 control-label">ความรู้ที่เรียน</label>
                            <div class="col-sm-9"><?php echo $student->learning;?></div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <h2 class="resume-title">ผลงาน</h2>
                    <div class="row">
                        <?php $resumeList = $this->MotherModel->getDynamicContent(19,1,$student_id);?>
                        <?php foreach ($resumeList->result_array() as $row){?>
                        <div class="col-md-3 col-sm-4 col-xs-6 resume-item">
                            <a href="<?php echo $row['image'];?>" class="fancybox"  rel="group">
                                <img src="<?php echo $row['image'];?>" alt="" class="img-responsive"/>
                            </a>
                        </div>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <br/>
</div>