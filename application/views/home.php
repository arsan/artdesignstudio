<div class="home">
    <?php $slider = $this->MotherModel->getDynamicContent(16,1,1);?>
    <div class="row slider">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <?php $count = 0; foreach ($slider->result_array() as $row){?>
                <li data-target="#carousel-example-generic" data-slide-to="<?php echo $count;?>" class="<?php echo ($count == 0)?'active':'';?>"></li>
                <?php $count++; }?>
            </ol>
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <?php $count = 0;foreach ($slider->result_array() as $row){?>
                <div class="item <?php echo ($count == 0)?'active':'';?>">
                    <a href="<?php echo ($row['link'] != '')?$row['link']:'#';?>">
                        <img src="<?php echo $row['image'];?>" alt="<?php echo $row['title'];?>" class="img-responsive">
                    </a>
                </div>
                <?php $count++; }?>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>
    </div>
    <div class="row course">
        <div class="col-md-2 col-sm-4 col-xs-6">
            <h2>COURSE</h2>
        </div>
        <?php $course = $this->MotherModel->getDynamicContent(10,1,1,5);?>
        <?php foreach ($course->result_array() as $row){?>
        <div class="col-md-2 col-sm-4 col-xs-6">
            <a href="<?php echo site_url('course#'.$row['title']);?>">
                <img src="<?php echo $row['thumb'];?>" alt="" class="img-responsive">
                <p class="title"><?php echo $row['title'];?></p>
            </a>
        </div>
        <?php }?>
    </div>
    <div class="row update">
        <div class="col-sm-12">
            <h2>UPDATE : Proud to Present</h2>
            <?php $news = $this->MotherModel->getDynamicContent(11,1,1,4,0);?>
            <div class="row">
                <?php foreach ($news->result_array() as $row){?>
                <div class="col-md-3 col-sm-6 col-xs-6 update-item">
                    <a href="<?php echo site_url('news/detail/'.$row['news_list_id']);?>">
                        <div class="img">
                            <img src="<?php echo $row['thumb'];?>" alt="" class="img-responsive">
                        </div>
                        <div class="content">
                            <p class="title"><?php echo $row['title'];?></p>
                            <p class="sub-title"><?php echo $row['sub_title'];?></p>
                            <p class="date"><?php echo $row['date'];?></p>
                        </div>
                        <div class="more">
                            Read more
                        </div>
                    </a>
                </div>
                <?php }?>
            </div>
            <div class="viewall"><a href="<?php echo site_url('news');?>">View All</a></div>
        </div>

    </div>
    <div class="row gallery">
        <div class="col-md-9 col-sm-12">
            <h2>GALLERY</h2>
            <?php $gallery = $this->MotherModel->getDynamicContent(17,1,1,3,0);?>
            <div class="row">
                <?php foreach ($gallery->result_array() as $row){?>
                <div class="col-sm-4  col-xs-6 gallery-list">
                    <a href="<?php echo site_url('gallery/detail/'.$row['home_gallery_id']);?>"><img src="<?php echo $row['thumb'];?>" alt="" class="img-responsive"></a>
                    <p><span class="ef4036">Artist:</span> <?php echo $row['artist'];?><br/>
                        <span class="ef4036">Media:</span> <?php echo $row['media'];?></p>
                </div>
                <?php }?>
            </div>
            <div class="viewall"><a href="<?php echo site_url('gallery');?>">View All</a></div>

            <h2>OUR STUDENTS</h2>
            <?php $students = $this->MotherModel->getDynamicContent(18,1,0,4,0);?>
            <div class="row">
                <?php foreach ($students->result_array() as $row){?>
                <div class="col-sm-3  col-xs-6 gallery-list">
                    <a href="<?php echo site_url('ourstudents/detail/'.$row['student_id']);?>"><img src="<?php echo $row['thumb'];?>" alt="" class="img-responsive"></a>
                    <p><span class="ef4036"><?php echo $row['fullname'];?></span> <br/>
                        <?php echo $row['school'];?></p>
                </div>
                <?php }?>
            </div>
            <div class="viewall"><a href="<?php echo site_url('ourstudents');?>">View All</a></div>
        </div>
        <div class="col-md-3 col-sm-12">
            <h2>&nbsp;</h2>
            <div class="behidetheframe">
                <h2>Behind The Frame</h2>
                <h3>By ครูพัลลภ (ครูเบิ้ม)</h3>
                <img src="<?php echo base_url('assets/imgs/home/behidetheframe.jpg');?>" alt="" class="img-responsive">
                <h3>วาดให้ง่าย วาดด้วยรูปทรงพื้นฐาน</h3>
                <p>
                    ปัจจุบันการเรียนวิชาสามัญในโรงเรียน มักจะให้เด็กๆวาดภาพประกอบบทเรียน เพื่อให้เกิดจินตนาการเป็นภาพ อันจะทำให้เข้าใจบทเรียนได้ง่ายขึ้น แต่การวาดภาพนั้นส่วนใหญ่จะสร้างปัญหาให้กับเด็ก หรือบางครั้งก็เลยมาถึงพ่อแม่ เพราะไม่รู้จะวาดภาพกันอย่างไร เพราะวาดไม่เป็นหรือไม่มีพรสวรรค์เอาเสียเลย</p>
                <a href="<? echo site_url('artstory');?>">More></a>
            </div>
        </div>

    </div>
</div>