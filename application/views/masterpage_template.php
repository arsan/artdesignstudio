<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php echo $this->ConfigModel->getMetaDescription(); ?>">
    <meta name="keyword" content="<?php echo $this->ConfigModel->getMetaKeyword(); ?>">
    <title><?php echo $this->ConfigModel->getWebsiteName(); ?></title>
    <link rel="icon" href="<?php echo $this->ConfigModel->getFavicon(); ?>">
    <!-- Normalize -->
    <link href="<?php echo base_url('assets/css/normalize.css');?>" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Style -->
    <link href="<?php echo base_url('assets/css/style.css');?>" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php echo $this->ConfigModel->getGoogleAnalytic(); ?>
  </head>
  <body>
    <?php echo $this->ConfigModel->getGoogleRemarketing(); ?>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?php echo base_url('assets/js/jquery.min.js');?>"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js');?>"></script>
    <!-- Form Validator -->
    <script src="<?php echo base_url('assets/js/plugins/formvalidator/jquery.form-validator.min.js'); ?>"></script>
    <script>$.validate();</script>
  </body>
</html>
